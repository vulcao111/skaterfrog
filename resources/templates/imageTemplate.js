module.exports = {
	name: 'templateName',
	description: 'templateDescription'+' . (prefixVartemplateName)',
	attributes: false,
	custom : true,
	execute(message,client) {
		client.isReady = false;
		const { MessageAttachment } = require('discord.js');
		const attachment = new MessageAttachment("templateURL");
		message.channel.send(attachment);
		client.isReady = true;
	},
};