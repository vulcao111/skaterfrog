echo off
:begin
cls
echo 1) Start
echo 2) Kill
echo 3) Dev
echo 0) Exit
set /p op=Option:
if "%op%"=="1" goto op1
if "%op%"=="2" goto op2
if "%op%"=="3" goto op3
if "%op%"=="4" goto op4
goto begin


:op1
echo Starting skater frog
cls
forever start index.js
goto begin

:op2
echo Stopping skater frog
cls
forever stopall
goto begin

:op3
echo Starting skater frog with nodemon
cls
nodemon index.js
goto begin

:op4
echo Goodbye
goto exit

:exit
cls
@exit