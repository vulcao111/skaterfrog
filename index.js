const {Client} = require('discord.js');
const config = require('./config/config.json');
const functions = require('./App/functions.js');
const isset = require('isset-php');
const moment = require('moment');
const fs = require('fs');
const db = require("./config/db");
const client = new Client();

client.on('ready', () => {
    console.log("["+moment().format('hh:mm:ss')+"] "+'Skater Frog is rolling.');
    client.user.setPresence({activity: {name: "rolling"}, status: 'invisible'}).then(r =>console.log("["+moment().format('hh:mm:ss')+"] "+"Presence set"));
});

async function executeCommand(message,server,name,attr){
    let commandFinal
    if(server === null){
        commandFinal = require('./App/Commands/'+name);
    }else{
        commandFinal = require('./App/Commands/'+server+'/'+name);
    }

    if(attr === null){
        commandFinal.execute(message,client)
    }else{
        commandFinal.execute(message,client,attr)
    }
}

client.on('message', message => {
    functions.data.checkPermissions(message).then(data=>{
        if(data['err'] === 0){
            functions.data.commandCheck(message).then(command=>{
                if(command['err'] === 0){
                    if(isset(() => command['attr'])){
                        try {
                            if(command['data']['custom'] === 0){
                                executeCommand(message,null,command['data']['Name'],command['attr']).then (r => console.log("["+moment().format('hh:mm:ss')+"] "+command['message']))
                            }else{
                                executeCommand(message,command['data']['ServerID'],command['data']['Name'],command['attr']).then (r => console.log("["+moment().format('hh:mm:ss')+"] "+command['message']))
                            }
                        } catch (error) {
                            console.error("["+moment().format('hh:mm:ss')+"] "+error);
                        }
                    }else{
                        try {
                            if(command['data']['custom'] === 0){
                                executeCommand(message,null,command['data']['Name'],null).then (r => console.log("["+moment().format('hh:mm:ss')+"] "+command['message']))
                            }else{
                                executeCommand(message,command['data']['ServerID'],command['data']['Name'],null).then (r => console.log("["+moment().format('hh:mm:ss')+"] "+command['message']))
                            }
                        } catch (error) {
                            console.error("["+moment().format('hh:mm:ss')+"] "+error);
                        }
                    }
                }
            })
        }else if((data['err'] === -1)){
            console.warn("["+moment().format('hh:mm:ss')+"] "+data['message']);
        }
    })
});

client.on('guildCreate', guild =>{ //whenever bot joins new server register
    db.runQuery("SELECT * FROM servers WHERE ServerID=?",[guild.id]).then(r =>{
        if(!isset(() => r[0])){
            db.runQuery("INSERT INTO servers (ServerID,Description,Owner,Prefix) VALUES (?,?,?,'*')",[guild.id,guild.name,guild.ownerID])
            fs.mkdir("./App/Commands/"+guild.id, (err)=>{
                if (err) {
                    throw err;
                }
                console.log("Server "+guild.name+" successfully registered.")
            })
        }else{
            console.log("Server "+guild.name+" is already registered.")
        }
    });
});

client.on('guildDelete', guild =>{ //whenever bot leaves server remove registration
    db.runQuery("SELECT * FROM servers WHERE ServerID=?",[guild.id]).then(r =>{
        if(isset(() => r[0])){
            db.runQuery("DELETE FROM servers WHERE ServerID = ?",[guild.id])
            db.runQuery("DELETE FROM commands WHERE ServerID = ?",[guild.id])
            db.runQuery("DELETE FROM silenced WHERE ServerID = ?",[guild.id])
            db.runQuery("DELETE FROM tictactoe WHERE ServerID = ?",[guild.id])
            fs.rmdir("./App/Commands/"+guild.id, { recursive: true } ,(err)=>{
                if (err) {
                    throw err;
                }
                console.log("Server "+guild.name+" successfully removed.")
            })
        }else{
            console.log("Server "+guild.name+" is not registered.")
        }
    });
});

client.login(config.token).then(r => console.log("["+moment().format('hh:mm:ss')+"] "+"Logged in"));