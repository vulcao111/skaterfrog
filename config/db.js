const mysql = require("mysql2/promise");

function defineDB () {
    return mysql.createPool({
        host:'localhost',
        user:'root',
        port: '3306',
        database: "skaterfrog"
    });
}

exports.runQuery = async function runQuery (sql, params = null) {
    const conn = defineDB();
    const data = await conn.query(sql, params)
        .then(resultados => { return resultados; })
    conn.end();
    return data[0];
};

exports.getName = mysql;