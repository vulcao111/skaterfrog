module.exports = {
	name: 'multiparams',
	description: 'Example of command with multiple parameters. (prefixVarmultiparams [text] [ntimes])',
	attributes: true,
	execute(message,client,params) {
		client.isReady = false;
		let text = ""
		let textEnd

		for (let i = 0 ; i < params.length; i++) {
			if(i !== params.length-2){
				text = text + params[i] + " "
			}else{
				text = text + params[i] + " "
				textEnd = i
				break;
			}
		}
		console.log(text)
		for (let i = 0; i < params[textEnd+1]; i++) {
			message.channel.send(text);
		}
		client.isReady = true;
	},
};