module.exports = {
	name: 'mDel',
	description: 'delete x number of messages. (prefixVarmDel [ammount min 0 max 100])',
	attributes: true,
	execute(message,client,params) {
		client.isReady = false;
		if(!isNaN(params[0])){
			if(params[0]>0 && params[0]<=100){
				message.channel.bulkDelete(Number(params[0]));
				message.channel.send(params[0]+' messages have been deleted')
			}
		}
		client.isReady = true;
	},
};