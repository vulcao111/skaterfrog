const db = require("../../config/db");
module.exports = {
	name: 'prefix',
	description: 'Change the prefix skaterfrog uses. (prefixVarprefix [$])',
	attributes: true,
	execute(message,client,params) {
		client.isReady = false;

		let text = ""

		for (let i = 0 ; i < params.length; i++) {
			if(i !== params.length-1){
				text = text + params[i] + " "
			}else{
				text = text + params[i]
			}
		}

		console.log(text)

		db.runQuery("UPDATE servers SET prefix = ? WHERE ServerID = ?",[text,message.guild.id]).then(value =>{
			message.channel.send("The prefix has been changed to '"+text+"' ." );
		}).catch(error =>{
			console.log(error)
		});

		client.isReady = true;
	},
};