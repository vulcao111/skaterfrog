//this is possibly the worst code i've ever written, don't look at it for too long for your own sake
const db = require("../../config/db");
const isset = require('isset-php');
module.exports = {
	name: 'ttt',
	description: 'Play tic tac toe with someone, (prefixVarttt new|-c|help [@example])',
	attributes: true,
	execute(message,client,params) {
		client.isReady = false;

		function winCheck(field,turn,value){ //win condition
			//turn is flipped due to update running before wincheck
			//split values between 3 arrays
			let main = [[field[0],field[1],field[2]],[field[3],field[4],field[5]],[field[6],field[7],field[8]]]

			for(let i=0;i<=2;i++){
				if((main[0][i] == 1 && main[1][i] == 1 && main[2][i] == 1) || (main[0][i] == 2 && main[1][i] == 2 && main[2][i] == 2)){ //verticals check
					if(turn == 1){
						//p1 wins
						db.runQuery("DELETE FROM tictactoe WHERE Player1 = ? OR Player2 = ?",[message.author.id,message.author.id])
						message.channel.send("The player "+value[0]['P1Username']+" has won!" );
					}else{
						//p2 wins
						db.runQuery("DELETE FROM tictactoe WHERE Player1 = ? OR Player2 = ?",[message.author.id,message.author.id])
						message.channel.send("The player "+value[0]['P2Username']+" has won!" );
					}
				}
				if((main[i][0] == 1 && main[i][1] == 1 && main[i][2] == 1) || (main[i][0] == 2 && main[i][1] == 2 && main[i][2] == 2)){ //horizontals check
					if(turn == 1){
						//p1 wins
						db.runQuery("DELETE FROM tictactoe WHERE Player1 = ? OR Player2 = ?",[message.author.id,message.author.id])
						message.channel.send("The player "+value[0]['P1Username']+" has won!" );
					}else{
						//p2 wins
						db.runQuery("DELETE FROM tictactoe WHERE Player1 = ? OR Player2 = ?",[message.author.id,message.author.id])
						message.channel.send("The player "+value[0]['P2Username']+" has won!" );
					}
				}
			}	

			if( ((main[0][0] == 1 && main[1][1] == 1 && main[2][2] == 1) || (main[0][2]==1 && main[1][1] == 1 && main[2][0] == 1)) || ((main[0][0] == 2 && main[1][1] == 2 && main[2][2] == 2) || (main[0][2]==2 && main[1][1] == 2 && main[2][0] == 2)) ){ //diagonal check
				if(turn == 1){
					//p1 wins
					db.runQuery("DELETE FROM tictactoe WHERE Player1 = ? OR Player2 = ?",[message.author.id,message.author.id])
					message.channel.send("The player "+value[0]['P1Username']+" has won!" );
				}else{
					//p2 wins
					db.runQuery("DELETE FROM tictactoe WHERE Player1 = ? OR Player2 = ?",[message.author.id,message.author.id])
					message.channel.send("The player "+value[0]['P2Username']+" has won!" );
				}
			}

			if((main[0][0] !== 0 && main[0][1] !== 0 && main[0][2] !== 0) && (main[1][0] !== 0 && main[1][1] !== 0 && main[1][2] !== 0) && (main[2][0] !== 0 && main[2][1] !== 0 && main[2][2] !== 0)){//draw check
				//p2 wins
				db.runQuery("DELETE FROM tictactoe WHERE Player1 = ? OR Player2 = ?",[message.author.id,message.author.id])
				message.channel.send("You both suck lmao" );
			}

		}

		//end of win condition

		function isInt(value) {
			return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
		}

		if(params[0] == "new"){
			if(params[1].id !== "707451362363047987" && message.author.id !== params[1].id){ //if not trying to fight himself or skaterfrog
				db.runQuery("select * from tictactoe where Player1=? OR Player2=?",[message.author.id,message.author.id]).then(value =>{
					if(!isset(() => value[0])){
						db.runQuery("INSERT INTO tictactoe (ServerID,Player1,Player2,P1Username,P2Username,turn) VALUES (?,?,?,?,?,?)",[message.guild.id,message.author.id,params[1].id,message.author.username,params[1].username,0]).then(r =>{
							db.runQuery("select * from tictactoe where Player1=? OR Player2=?",[message.author.id,message.author.id]).then(value =>{
								let field = []
								let text
								text = value[0]['P1Username']+"**(*)** :large_blue_diamond:"+" :crossed_swords: "+":red_circle: "+value[0]['P2Username']+"\n"
								for(let i=1;i<=9;i++){
									field[i-1] = value[0]['f'+i]
									if(value[0]['f'+i] == 1){
										play = ":large_blue_diamond:"
									}else if(value[0]['f'+i] == 2){
										play = ":red_circle:"
									}else{
										play = ":black_medium_square:"
									}
									if(i==3 || i==6){
										text += play+"\n"
									}else{
										text += play
									}
								}
								message.channel.send("A new game has been created between "+message.author.username+" and "+params[1].username+"!" );

								message.channel.send(text);
							}).catch(error =>{
								console.log(error)
							});
						})	
					}else{
						message.channel.send("You're already in a game.");
					}			
				}).catch(error =>{
					console.log(error)
				});				
			}else{
				message.channel.send("no." );
			}
		}else if(isInt(params[0])){
			let allowed = [1,2,3,4,5,6,7,8,9]
			if(allowed.indexOf(params[0]>0)){ //if play is between 1 and 9
				db.runQuery("select * from tictactoe where Player1=? OR Player2=?",[message.author.id,message.author.id]).then(value =>{ //get game between both players
					if(message.author.id == value[0].Player1){//player 1 turn
						if(value[0].turn == 0){ //if it's player 1's turn
							if(value[0]["f"+params[0]] == 0){ //if spot is empty
								db.runQuery("UPDATE tictactoe SET f"+params[0]+"=1,turn=1 WHERE Player1=? OR Player2=?",[message.author.id,message.author.id]).then(r =>{
									db.runQuery("select * from tictactoe where Player1=? OR Player2=?",[message.author.id,message.author.id]).then(value =>{
										let field = []
										let text
										text = value[0]['P1Username']+" :large_blue_diamond:"+" :crossed_swords: "+":red_circle: **(*)**"+value[0]['P2Username']+"\n"
										for(let i=1;i<=9;i++){
											field[i-1] = value[0]['f'+i]
											if(value[0]['f'+i] == 1){
												play = ":large_blue_diamond:"
											}else if(value[0]['f'+i] == 2){
												play = ":red_circle:"
											}else{
												play = ":black_medium_square:"
											}
											if(i==3 || i==6){
												text += play+"\n"
											}else{
												text += play
											}
										}
						
										message.channel.send(text);

										winCheck(field,value[0].turn,value)						
									}).catch(error =>{
										console.log(error)
									});
								})
							}
						}else{
							message.channel.send("It's not your turn yet." );
						}
					}else if(message.author.id == value[0].Player2){ //is it player 2
						if(value[0].turn == 1){ //is it player 2's turn
							if(value[0]["f"+params[0]] == 0){ //check if empty
								db.runQuery("UPDATE tictactoe SET f"+params[0]+"=2,turn=0 WHERE Player1=? OR Player2=?",[message.author.id,message.author.id]).then(r =>{
									db.runQuery("select * from tictactoe where Player1=? OR Player2=?",[message.author.id,message.author.id]).then(value =>{
										let field = []
										let text
										text = value[0]['P1Username']+"**(*)** :large_blue_diamond:"+" :crossed_swords: "+":red_circle: "+value[0]['P2Username']+"\n"
										for(let i=1;i<=9;i++){
											field[i-1] = value[0]['f'+i]
											if(value[0]['f'+i] == 1){
												play = ":large_blue_diamond:"
											}else if(value[0]['f'+i] == 2){
												play = ":red_circle:"
											}else{
												play = ":black_medium_square:"
											}
											if(i==3 || i==6){
												text += play+"\n"
											}else{
												text += play
											}
										}
						
										message.channel.send(text);
		
										winCheck(field,value[0].turn,value)
									}).catch(error =>{
										console.log(error)
									});
								})							
							}
						}else{
							message.channel.send("It's not your turn yet." );	
						}	
					} //not playing
				}).catch(error =>{
					console.log(error)
				});
			}
		}else if(params[0] == "-c"){
			db.runQuery("DELETE FROM tictactoe WHERE Player1 = ? OR Player2 = ?",[message.author.id,message.author.id]).then(r =>{
				if(r.affectedRows !== 0){
					message.channel.send("Match cancelled" );
				}
			})
		}else if(params[0] == "help"){
			let text = "How to play:\n\nUse the numbers from 1 to 9 after the command to play in the space the number represents\nType -c after the command to cancel the ongoing game\nYou can't play more than one game at a time";
			message.channel.send(text);
		}
	
		client.isReady = true;
	},
};