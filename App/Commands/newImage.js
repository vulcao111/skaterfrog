const db = require("../../config/db");
const isset = require('isset-php');

module.exports = {
	name: 'newImage',
	description: 'create a command to send an image of your liking. (prefixVarnewimage [name] [description] [url])',
	attributes: true,
	execute(message,client, params) {
		client.isReady = false;
		const fs = require('fs');
		let template = fs.readFileSync('./resources/templates/imageTemplate.js').toString()

		template = template.replace('templateName',""+params[0])
		template = template.replace('templateName',""+params[0])

		let desc = ""
		let descEnd

		for (let i = 1 ; i < params.length; i++) {
			if(i !== params.length-2){
				desc = desc + params[i] + " "
			}else{
				desc = desc + params[i]
				descEnd = i
				break
			}
		}

		template = template.replace('templateDescription',desc)
		template = template.replace('templateURL',params[descEnd+1])
		db.runQuery("SELECT * FROM commands WHERE Name=?", [params[0]]).then(r =>{
			if(!isset(() => r[0])){
				db.runQuery("INSERT INTO commands (`ServerID`,`Name`,`Description`,`custom`,`attributes`,`state`) VALUES (?,?,?,?,?,?)", [message.guild.id, params[0], desc + '. (prefixVar' + params[0] + ')', 1, false, 1 ]).then(r =>{
					fs.writeFile('App/Commands/'+message.guild.id+'/'+params[0]+'.js', template, function (err) {
						if (err) return console.log(err);
						console.log('Command '+params[0]+' created');
					});
					message.channel.send("The custom command "+params[0]+" has been created." );
					client.isReady = true
				})
			}else{
				message.channel.send("The command already exists.");
				client.isReady = true
			}
		})
	},
};