const db = require("../../config/db");
module.exports = {
	name: 'delImage',
	description: 'delete an image command. (prefixVardelImage [name])',
	attributes: true,
	execute(message,client, params) {
		client.isReady = false;
		const fs = require('fs');
		let command
		let flag = true

		try {
			command = fs.readFileSync('./App/Commands/'+message.guild.id+"/"+params[0]+'.js').toString()
		} catch (error) {
			flag=false
		}

		if(flag){
			db.runQuery("DELETE FROM commands WHERE ServerID=? AND Name=?", [message.guild.id, params[0]]).then(r =>{
				fs.unlink('./App/Commands/'+message.guild.id+"/"+params[0]+'.js', (err) => {
					if (err) {
						throw err;
					}
					console.log("Command "+params[0]+" has been deleted.");
				});
				message.channel.send("The custom command "+params[0]+" has been deleted.");
			})
		}else{
			message.channel.send("Command "+params[0]+" does not exist");
		}
		client.isReady = true;
	},
};