const db = require("../../config/db");
module.exports = {
	name: 'help',
	description: 'Shows info on all commands. (prefixVarhelp)',
	attributes: false,
	async execute(message, client) {
		let prefix = await db.runQuery("SELECT prefix FROM servers WHERE ServerID=?", [message.guild.id])
		prefix = prefix[0]['prefix']
		let commands = await db.runQuery("SELECT * FROM commands WHERE ServerID=? OR ServerID='global' AND state=1 ORDER BY id asc", [message.guild.id])
		client.isReady = false;
		let content = {
			color: "#00C000",
			title: "Info:",
			fields: []
		};

		for (let i = 0; i < commands.length; i++) {
			content.fields.push({
				name: commands[i]['custom']===0?commands[i]['Name']:commands[i]['Name']+' **(custom)**',
				value: commands[i]['Description'].replace('prefixVar', prefix),
				inline: false
			})
		}
		await message.channel.send({embed: content});
		client.isReady = true;
	},
};