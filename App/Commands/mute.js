const db = require("../../config/db");
const isset = require('isset-php');
module.exports = {
	name: 'mute',
	description: 'Mutes/Unmutes user/channel/server. (prefixVarmute [@example|-c|-s])',
	attributes: true,
	execute(message,client,target) {
		client.isReady = false;
		if(target[0]=="-s"){
			db.runQuery("select * from silenced where Type='Server' AND ServerID=?",[message.guild.id]).then(value =>{
				if(!isset(() => value[0])){
					db.runQuery("INSERT INTO silenced (Type,ServerID,MuterID) VALUES (?,?,?)",["Server",message.guild.id,message.author.id])
					message.channel.send("The server has been muted." );
				}else{
					db.runQuery("DELETE FROM silenced WHERE Type='Server' AND ServerID = ?",[message.guild.id])
					message.channel.send("The server has been unmuted." );
				}
			}).catch(error =>{
				console.log(error)
			});
		}else if(target[0]=="-c"){
			db.runQuery("select * from silenced where Type='Channel' AND ServerID=? AND ChannelID = ?",[message.guild.id,message.channel.id]).then(value =>{
				if(!isset(() => value[0])){
					db.runQuery("INSERT INTO silenced (Type,ChannelID,ServerID,MuterID) VALUES (?,?,?,?)",["Channel",message.channel.id,message.guild.id,message.author.id])
					message.channel.send("The channel "+message.channel.name +" has been muted." );
				}else{
					db.runQuery("DELETE FROM silenced WHERE Type='Channel' AND ChannelID = ? AND ServerID = ?",[message.channel.id,message.guild.id])
					message.channel.send("The channel "+message.channel.name +" has been unmuted." );
				}
			}).catch(error =>{
				console.log(error)
			});
		}else if(!isNaN(target[0]) && target[0].length == 18){
			db.runQuery("select * from silenced where UserID=? AND ServerID=?",[target[0],message.guild.id]).then(value =>{
				let flag = false
				for(let row of value){
					if(row.UserID === target[0]){
						flag = true
						break;
					}
				}
				if(!flag){
					db.runQuery("INSERT INTO silenced (Type,UserID,ServerID,MuterID) VALUES (?,?,?,?)",["User",target[0],message.guild.id,message.author.id])
					message.author.send("<@!"+target[0]+"> has been muted")
				}else{
					db.runQuery("DELETE FROM silenced WHERE UserID = ? AND ServerID = ?",[target[0],message.guild.id])
					message.author.send("<@!"+target[0]+"> has been unmuted")
				}
			}).catch(error =>{
				console.log(error)
			});
		}else if(isset(() => target[0].id)){
			db.runQuery("select * from silenced where UserID=? AND ServerID=?",[target[0].id,message.guild.id]).then(value =>{
				let flag = false
				for(let row of value){
					if(row.UserID === target[0].id){
						flag = true
						break;
					}
				}
				if(!flag){
					db.runQuery("INSERT INTO silenced (Type,UserID,ServerID,MuterID) VALUES (?,?,?,?)",["User",target[0].id,message.guild.id,message.author.id])
					message.channel.send("The user "+target[0].username +" has been muted." );
				}else{
					db.runQuery("DELETE FROM silenced WHERE UserID = ? AND ServerID = ?",[target[0].id,message.guild.id])
					message.channel.send("The user "+target[0].username +" has been unmuted." );
				}
			}).catch(error =>{
				console.log(error)
			});
		}

		client.isReady = true;
	},
};