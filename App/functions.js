const db = require("../config/db");
const isset = require('isset-php');
const methods = {
	commandCheck: async function commandCheck(message) {
		let content = message.content
		let data = [];
		let explode = content.split(" ")

		let commands = await db.runQuery("SELECT * FROM commands WHERE ServerID=? OR ServerID='global' AND state=1 ORDER BY id asc", [message.guild.id])

		let prefix = await db.runQuery("SELECT prefix FROM servers WHERE ServerID=?", [message.guild.id])
		prefix = prefix[0]['prefix']

		function checkPrefixAndCommand(){
			if (content.substring(0,prefix.length) === prefix) {
				explode[0] = content.substring(0,prefix.length)
				explode[1] = content.substring(prefix.length,content.length)
			}
			return !!(explode[0] === prefix && isset(() => explode[1]))
		}

		if (checkPrefixAndCommand()) {
			let params = [];
			let temp = explode[1].split(" ")
			explode.shift()
			explode[0] = prefix
			explode = temp
			if (isset(() => explode[1])) {
				for (let i = 1; i < explode.length; i++) {
					if (explode[i].startsWith("<@")) {
						params.push(message.mentions.users.first())
					} else {
						params.push(explode[i])
					}
				}
			}

			let command = undefined

			for (let i = 0; i < commands.length; i++) {
				if(commands[i].Name === explode[0]){
					command = commands[i]
					break;
				}
			}

			if (command !== undefined) {
				if (isset(() => params[0]) && (command.attributes)) {
					data['data'] = command
					data['attr'] = params
					data['message'] = "Command " + content + " was executed."
					data['err'] = 0
					return data;
				} else if (!isset(() => params[1]) && (command.attributes)) {
					data['data'] = command
					data['message'] = "Command " + content + " is missing attributes."
					data['err'] = -1
					return data;
				} else if (!command.attributes && !isset(() => params[0])) {
					data['data'] = command
					data['message'] = "Command " + content + " was executed."
					data['err'] = 0
					return data;
				}
			} else {
				data['data'] = ""
				data['message'] = "Command " + content + " is invalid."
				data['err'] = -1
				return data;
			}
		} else {
			if(content === "skaterfrog") {
				message.channel.send("I'm currently using the prefix: '" + prefix + "'.");
				data['data'] = ""
				data['message'] = "I'm currently using the prefix: '" + prefix + "'."
				data['err'] = -1
				return data;
			}else{
				data['data'] = ""
				data['message'] = "Not a command."
				data['err'] = -1
				return data;
			}
		}
	},

	checkPermissions: async function checkPermissions(message){
		let data = [];
		if(message.author.id !== "707451362363047987" && isset(() => message.guild)){
			const muted = await db.runQuery("select * from silenced where ServerID=?",[message.guild.id])
			let flag = false
			for(let row of muted){
				if((row.UserID === message.author.id && row.MuterID !== message.author.id) || (row.ServerID === message.guild.id && row.Type === "Server" && row.MuterID !== message.author.id) || (row.ServerID === message.guild.id && row.Type === "Channel" && row.ChannelID === message.channel.id && row.MuterID !== message.author.id)){
					flag = true
					break
				}
			}

			if(!flag){
				data['data'] = ""
				data['message'] = ""
				data['err'] = 0
				return data;
			}else{
				data['data'] = ""
				data['message'] = message.author.username + " is silenced."
				data['err'] = -1
				message.delete({ timeout: 0 });
				return data;
			}

		}else{
			data['data'] = ""
			data['message'] = ""
			data['err'] = -2
			return data;
		}
	},
};

exports.data = methods;